#pragma once
#include "StackOverflowException.h"
#include "StackUnderflowException.h"


namespace StackExercise
{
	template<typename T, size_t SIZE>
	class TStack
	{
	public:
		TStack(): m_nextIndex(0)
		{}

		void Push(T val)
		{
			if (m_nextIndex < SIZE)
			{
				m_impl[m_nextIndex] = val;
				++m_nextIndex;
			}
			else
			{
				throw StackOverflowException();
			}
		}
		T Pop()
		{
			if (m_nextIndex > 0)
			{
				--m_nextIndex;
				return m_impl[m_nextIndex];
			}
			else
			{
				throw StackUnderflowException();
			}
		}

	private:
		T m_impl[SIZE];
		int m_nextIndex;
	};
}