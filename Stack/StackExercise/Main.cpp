// Israel yurman
#include <iostream>
#include "TStack.h"

void main()
{
	const size_t SIZE = 10;
	StackExercise::TStack<int, SIZE> int_s;
	StackExercise::TStack<double, SIZE> double_s;

	try
	{
		for (size_t i = 0; i < SIZE; i++)
		{
			int_s.Push(i);
			double_s.Push(i + 0.1);
		}

		for (size_t i = 0; i < SIZE; i++)
		{
			std::cout << "int stack index "<< i << " is: " << int_s.Pop() << std::endl;
			std::cout << "double stack index " << i << " is: " << double_s.Pop() << std::endl;
		}
	}
	catch (const std::exception&)
	{

	}
	system("pause");
}
